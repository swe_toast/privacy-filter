# Here is the offical stance on contributing to this project

1. New functions if submitted by shell script has to be in code blocks (see [example below](#example-of-a-code-block)), since thats the syntax the project uses.
2. New ideas can be submitted as a issue posted on tracker and the author will be given credit in the script on the contributer line.
3. Make sure that it bases of the latest revision.

## example of a code block
````shell
set_value=0 #this is the description of the setting

info_test () {
echo "$set_value"
}

function_test () {
info_test    
}

function_test
````

there are the differant types for submitting a code block so check the official guidelines for naming a new function below then `function_` is used to bind them up to the script.

### info_
> used for general information displaying.

### block_
> used for ipset, iptables basicly firewall features.

### setting_
> used for checking or verifying a setting.

### run_
> used for sorting the list.

### set_
> Adjustable variable settings should be defined by its function and a small description using a # hashbang preciding the description.

### non_
> is used by non adjustable varible settings should also be defined by its function.