# Privacy-Filter
<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons License" align="right" hspace="20" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a>
**Description**: This script blocks [Telemetry](https://technet.microsoft.com/itpro/windows/configure/basic-level-windows-diagnostic-events-and-fields), [Shodan.io crawlers](https://www.shodan.io/) and an [Android Rootkit](https://www.kb.cert.org/vuls/id/624539), it supports both ipv4 and ipv6 out of the box however ipv6 blocking will only work on routers with [ipset version 6](https://github.com/RMerl/asuswrt-merlin/wiki/Using-ipset#ipset-version-and-router-models) installed.

* Enable and format [JFFS](https://github.com/RMerl/asuswrt-merlin/wiki/JFFS) through WEB UI first (if not already enabled)

* Then place [**this content**](https://gitlab.com/swe_toast/privacy-filter/raw/master/privacy-filter) in the file:  `/jffs/scripts/privacy-filter`

* Then make it executable:

```shell
chmod +x /jffs/scripts/privacy-filter
```
* Optional is to set a whitelist if you want to exclude domains in the file: `/jffs/privacy-filter-whitelist.list`

* Then append the following line to /jffs/scripts/services-start:

```shell
cru a privacy-filter "0 */12 * * * /jffs/scripts/privacy-filter"
```

* This will make privacy-filter run on a schedule it will run every 12th hour, to verify that the entry works after its added just type:

```shell
cru l
```

* Finally call this at the end of your existing /jffs/scripts/firewall-start:

```shell
# Load ipset filter rules
sh /jffs/scripts/privacy-filter
```

* To run this manually just type this command:

```shell
/jffs/scripts/privacy-filter
```

* Privacy filter will also print to syslog so you dont have to check ssh to see if its working it should read something like this:

```
Apr  1 00:00:06 system: Privacy Filter (ipv4) loaded 115 unique ip addresses that will be rejected from contacting your  router.
```

**Additional Notes**: DD-WRT users should replace iptables-save to iptables -L in order to use this script, DD-WRT/OpenWRT/Padavan users must [change path](https://gitlab.com/swe_toast/privacy-filter/blob/master/privacy-filter#L7) for this script to work to /opt/bin instead of /jffs/scripts/ for all paths in the installation instructions an in the script.

If Asuswrt-Merlin or Asuswrt users run into issues, there is a [debug tool](https://gitlab.com/swe_toast/debugtool/raw/master/debugtool.sh) available to help both developer and end user to figure out what the issue is. Here is how to use the debug tool then paste the link that the script produces at the end in the thread when you seek support.

```shell
 wget https://gitlab.com/swe_toast/debugtool/raw/master/debugtool.sh && sh debugtool.sh  
```

For support on this script please use the issue tracker.